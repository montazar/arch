#!/usr/bin/env bash
#-------------------------------------------------------------------------
#   █████╗ ██████╗  ██████╗██╗  ██╗████████╗██╗████████╗██╗   ██╗███████╗
#  ██╔══██╗██╔══██╗██╔════╝██║  ██║╚══██╔══╝██║╚══██╔══╝██║   ██║██╔════╝
#  ███████║██████╔╝██║     ███████║   ██║   ██║   ██║   ██║   ██║███████╗
#  ██╔══██║██╔══██╗██║     ██╔══██║   ██║   ██║   ██║   ██║   ██║╚════██║
#  ██║  ██║██║  ██║╚██████╗██║  ██║   ██║   ██║   ██║   ╚██████╔╝███████║
#  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝   ╚═╝    ╚═════╝ ╚══════╝
#-------------------------------------------------------------------------
echo "--------------------------------------"
echo "--          Network Setup           --"
echo "--------------------------------------"
pacman -S networkmanager dhclient --noconfirm --needed
systemctl enable --now NetworkManager
echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download          "
echo "-------------------------------------------------"
pacman -S --noconfirm pacman-contrib curl
pacman -S --noconfirm reflector rsync
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak

nc=$(grep -c ^processor /proc/cpuinfo)
echo "You have " $nc" cores."
echo "-------------------------------------------------"
echo "Changing the makeflags for "$nc" cores."
TOTALMEM=$(cat /proc/meminfo | grep -i 'memtotal' | grep -o '[[:digit:]]*')
if [[  $TOTALMEM -gt 8000000 ]]; then
    sed -i "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$nc\"/g" /etc/makepkg.conf
    echo "Changing the compression settings for "$nc" cores."
    sed -i "s/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T $nc -z -)/g" /etc/makepkg.conf
fi
echo "-------------------------------------------------------------------------"
echo "--               Setup Language to US and set locale                   --"
echo "-------------------------------------------------------------------------"
timezone=$(curl -4 http://ip-api.com/line?fields=timezone)
locale=en_US.UTF-8

# Set locale
sed -i s/^#$locale/$locale/ /etc/locale.gen
locale-gen
echo LANG=$locale  > /etc/locale.conf
localectl --no-ask-password set-locale LANG=$locale LC_TIME=$locale

# Set time
timedatectl --no-ask-password set-ntp 1
timedatectl --no-ask-password set-timezone $timezone

# Set keymaps
localectl --no-ask-password set-keymap us
    
# Set timezone when NetworkManager connects to a network
file=/etc/NetworkManager/dispatcher.d/09-timezone.sh
cat <<EOF > $file
#!/bin/sh
case "\$2" in
    up)
    	timezone=\$(curl --fail http://ip-api.com/line?fields=timezone)
        timedatectl --no-ask-password set-timezone \$timezone
	#ln -sf /usr/share/zoneinfo/\$timezone /etc/localtime
    ;;
esac
EOF
chmod +x $file

# Add sudo no password rights
sed -i 's/^# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

#Add parallel downloading
sed -i 's/^#Para/Para/' /etc/pacman.conf

#Enable multilib
sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
pacman -Sy --noconfirm

echo "-------------------------------------------------------------------------"
echo "--                  Installing Requested Packages                      --"
echo "-------------------------------------------------------------------------"
PKGS=(
'mesa' # Essential Xorg First
'xorg'
'xorg-server'
'xorg-apps'
'xorg-drivers'
'xorg-xkill'
'xorg-xinit'
'xterm'
'plasma-desktop' # KDE Load second
'alsa-plugins' # audio plugins
'alsa-utils' # audio utils
'alacritty'
'ark' # compression
'audiocd-kio' 
'autoconf' # build
'automake' # build
'base'
'bash-completion'
'bind'
'binutils'
'bison'
'bluedevil'
'bluez'
'bluez-libs'
'bluez-utils'
'breeze'
'breeze-gtk'
'bridge-utils'
'btrfs-progs'
'cronie'
'cups'
'dialog'
'discover'
'dolphin'
'dosfstools'
'dtc'
'efibootmgr' # EFI boot
'egl-wayland'
'exfat-utils'
'extra-cmake-modules'
'filelight'
'flex'
'fuse2'
'fuse3'
'fuseiso'
'gcc'
'gimp' # Photo editing
'git'
'gparted' # partition management
'gptfdisk'
'grub'
'grub-customizer'
'gst-libav'
'gst-plugins-good'
'gst-plugins-ugly'
'gwenview'
'haveged'
'htop'
'iptables-nft'
'jdk-openjdk' # Java 17
'kate'
'kcodecs'
'kcoreaddons'
'kdeplasma-addons'
'kde-gtk-config'
'kinfocenter'
'kscreen'
'kvantum-qt5'
'konsole'
'kscreen'
'layer-shell-qt'
'libdvdcss'
'libnewt'
'libtool'
'linux'
'linux-firmware'
'linux-headers'
'lsof'
'lutris'
'lzop'
'm4'
'make'
'milou'
'nano'
'networkmanager'
'ntfs-3g'
'ntp'
'okular'
'openbsd-netcat'
'openssh'
'os-prober'
'oxygen'
'p7zip'
'pacman-contrib'
'patch'
'packagekit-qt5'
'picom'
'pkgconf'
'plasma-meta'
'plasma-nm'
'powerdevil'
'powerline-fonts'
'print-manager'
'pulseaudio'
'pulseaudio-alsa'
'pulseaudio-bluetooth'
'python-notify2'
'python-psutil'
'python-pyqt5'
'python-pip'
'qemu'
'rsync'
'sddm'
'sddm-kcm'
'snapper'
'spectacle'
'steam'
'sudo'
'swtpm'
'synergy'
'systemsettings'
'terminus-font'
'traceroute'
'ufw'
'unrar'
'unzip'
'usbutils'
'vim'
'virt-manager'
'virt-viewer'
'wget'
'which'
'wine-gecko'
'wine-mono'
'winetricks'
'xdg-desktop-portal-kde'
'xdg-user-dirs'
'zeroconf-ioslave'
'zip'
'zsh'
'zsh-syntax-highlighting'
'zsh-autosuggestions'
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

echo "-------------------------------------------------------------------------"
echo "--                 Installing Processor Microcode                      --"
echo "-------------------------------------------------------------------------"
proc_type=$(lscpu | awk '/Vendor ID:/ {print $3}')
case "$proc_type" in
    GenuineIntel)
        echo "Installing Intel microcode"
        pacman -S --noconfirm intel-ucode
        proc_ucode=intel-ucode.img
        ;;
    AuthenticAMD)
        echo "Installing AMD microcode"
        pacman -S --noconfirm amd-ucode
        proc_ucode=amd-ucode.img
        ;;
esac	


echo "-------------------------------------------------------------------------"
echo "--                   Installing Grapics Drivers                        --"
echo "-------------------------------------------------------------------------"
if lspci | grep -E "NVIDIA|GeForce"; then
    echo "Installing NVIDIA Drivers."
    pacman -S nvidia --noconfirm --needed
    nvidia-xconfig
elif lspci | grep -E "Radeon"; then
    echo "Installing ATI/AMD Drivers."
    pacman -S xf86-video-amdgpu --noconfirm --needed
elif lspci | grep -E "Integrated Graphics Controller"; then
    echo "Installing Intel Integrated Drivers."
    pacman -S libva-intel-driver libvdpau-va-gl lib32-vulkan-intel vulkan-intel libva-intel-driver libva-utils --needed --noconfirm
fi

hypervisor=$(systemd-detect-virt)
case $hypervisor in
kvm )		echo "Installing KVM guest tools."
		pacman -S qemu-guest-agent --noconfirm --needed
		systemctl enable qemu-guest-agent
		;;
vmware )   	echo "Installing VMWare guest tools."
	    	pacman -S open-vm-tools --noconfirm --needed
	    	systemctl enable vmtoolsd
	    	systemctl enable vmware-vmblock-fuse
	    	;;
oracle )    	echo "Installing VirtualBox guest tools."
	    	pacman -S virtualbox-guest-utils --noconfirm --needed
		systemctl enable vboxservice
	    	;;
microsoft ) 	echo "Installing Hyper-V guest tools."
		pacman -S hyperv --noconfirm --needed
	    	systemctl enable hv_fcopy_daemon
	    	systemctl enable hv_kvp_daemon
	    	systemctl enable hv_vss_daemon
	    	;;
esac

if ! source install.conf; then
	read -p "Please enter username: " username
echo "username=$username" >> ${HOME}/ArchTitus/install.conf
fi
if [ $(whoami) = "root"  ];
then
    useradd -m -G wheel,libvirt -s /bin/bash $username 
	passwd $username
	cp -R /root/ArchTitus /home/$username/
    chown -R $username: /home/$username/ArchTitus
	read -p "Please name your machine:" nameofmachine
	echo $nameofmachine > /etc/hostname
else
	echo "You are already a user proceed with aur installs"
fi

