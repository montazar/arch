#!/usr/bin/env bash
#-------------------------------------------------------------------------
#   █████╗ ██████╗  ██████╗██╗  ██╗████████╗██╗████████╗██╗   ██╗███████╗
#  ██╔══██╗██╔══██╗██╔════╝██║  ██║╚══██╔══╝██║╚══██╔══╝██║   ██║██╔════╝
#  ███████║██████╔╝██║     ███████║   ██║   ██║   ██║   ██║   ██║███████╗
#  ██╔══██║██╔══██╗██║     ██╔══██║   ██║   ██║   ██║   ██║   ██║╚════██║
#  ██║  ██║██║  ██║╚██████╗██║  ██║   ██║   ██║   ██║   ╚██████╔╝███████║
#  ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝   ╚═╝    ╚═════╝ ╚══════╝
#-------------------------------------------------------------------------

echo -e "\nFINAL SETUP AND CONFIGURATION"
echo "--------------------------------------"
echo "-- GRUB EFI Bootloader Install&Check--"
echo "--------------------------------------"
if [[ -d "/sys/firmware/efi" ]]; then
    grub-install --efi-directory=/boot ${DISK}
fi
grub-mkconfig -o /boot/grub/grub.cfg

# ------------------------------------------------------------------------

echo -e "\nEnabling Login Display Manager"
systemctl enable sddm.service
echo -e "\nSetup SDDM Theme"
cat <<EOF > /etc/sddm.conf
[Theme]
Current=Nordic
EOF

# ------------------------------------------------------------------------

echo -e "\nEnabling essential services"

systemctl enable cups.service
ntpd -qg
systemctl enable ntpd.service
systemctl disable dhcpcd.service
systemctl stop dhcpcd.service
systemctl enable NetworkManager.service
systemctl enable bluetooth

yay -S --noconfirm --needed cifs-utils

read -p "Please enter username: " username

read -rp 'Enter IP address: '
ip_address="$REPLY"

read -rp 'Enter volume name: '
volume_name="$(echo $REPLY | sed 's/ /\\040/g')"

read -rp 'Enter mount path: '
mount_path="/home/${username}/$REPLY"

echo 'WARNING: YOUR PASSWORD WILL STORED IN PLAINTEXT'
while :; do
    read -rsp 'Enter your password: '
    password="$REPLY"
    echo
    read -rsp 'Confirm password: '

    if [ "${password}" = "$REPLY" ]; then
        echo
        break;
    fi

    echo
    echo
    echo 'Password do not match. Try again.'

done
echo

while :; do
    read -rp 'Enter your server username: '
    server_username="$REPLY"
    echo
    read -rp 'Confirm server username: '

    if [ "${server_username}" = "$REPLY" ]; then
        echo
        break;
    fi

    echo
    echo
    echo 'Username do not match. Try again.'

done

while :; do
    read -rp 'Enter your DOMAIN or WORKGROUP: '
    domain="$REPLY"
    echo
    read -rp 'Confirm DOMAIN or WORKGROUP: '

    if [ "${domain}" = "$REPLY" ]; then
        echo
        break;
    fi

    echo
    echo
    echo 'DOMAIN or WORKGROUP do not match. Try again.'

done

cat > "/home/${username}/.smbcredentials" << EOF
username=${server_username}
password=${password}
domain=${domain}
EOF

mkdir -p "$mount_path"
sudo chown root:root "/home/${username}/.smbcredentials"
sudo chmod 600 "/home/${username}/.smbcredentials"

if ! grep "//${ip_address}/${mount_path}" /etc/fstab; then
    sudo tee -a /etc/fstab << EOF
//${ip_address}/${volume_name} ${mount_path} cifs _netdev,credentials=/home/${username}/.smbcredentials,uid=1000,gid=1000,iocharset=utf8,actimeo=3,noatime,vers=3.0 0 0
EOF
fi

sudo ln -s $(which vim) /usr/bin/vi

echo "
###############################################################################
# Cleaning
###############################################################################
"

# Remove no password sudo rights
sed -i 's/^%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
# Add sudo rights
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# Disable password prompt timeout
printf 'Defaults passwd_timeout=0' >> /etc/sudoers

# Replace in the same state
cd $pwd
echo "
###############################################################################
# Done - Please Eject Install Media and Reboot
###############################################################################
"
